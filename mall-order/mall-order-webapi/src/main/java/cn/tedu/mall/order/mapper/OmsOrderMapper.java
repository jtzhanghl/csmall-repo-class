package cn.tedu.mall.order.mapper;

import cn.tedu.mall.pojo.order.model.OmsOrder;
import cn.tedu.mall.pojo.order.vo.OrderListVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OmsOrderMapper {

    // 新增订单的方法
    int insertOrder(OmsOrder order);

    // 查询当前登录用户指定时间范围内所有订单
    List<OrderListVO> selectOrderBetweenTimes(
            @Param("userId") Long userId,
            @Param("startTime") LocalDateTime startTime,
            @Param("endTime") LocalDateTime endTime);

    // 根据动态sql,实现订单对象的动态修改
    // 参数omsOrder对象,其中包含哪个属性的值,就修改哪个对应的列
    // 注意:id必须有值,并不是所有列都能修改
    int updateOrderById(OmsOrder order);



}
