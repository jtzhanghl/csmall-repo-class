package cn.tedu.mall.order.mapper;

import cn.tedu.mall.pojo.order.model.OmsOrderItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OmsOrderItemMapper {

    // 新增订单项(oms_order_item)的方法
    // 一个订单可能包含多个订单项,也就是一个订单包含多个商品
    // 如果一个订单中包含的商品较多,就需要多次连接数据库完成新增订单项的操作
    // 如果我们想减少连库次数提高效率,就需要提供一个能够一次新增多条数据的方法
    // 这里也使用动态sql语句实现
    int insertOrderItemList(List<OmsOrderItem> omsOrderItems);

}





