package cn.tedu.mall.front.service.impl;

import cn.tedu.mall.common.restful.JsonPage;
import cn.tedu.mall.front.service.IFrontProductService;
import cn.tedu.mall.pojo.product.vo.*;
import cn.tedu.mall.product.service.front.IForFrontAttributeService;
import cn.tedu.mall.product.service.front.IForFrontSkuService;
import cn.tedu.mall.product.service.front.IForFrontSpuService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class FrontProductServiceImpl implements IFrontProductService {

    // Dubbo调用product模块按分类id查询spu列表的功能
    @DubboReference
    private IForFrontSpuService dubboSpuService;

    // 根据spuId查询sku列表的对象
    @DubboReference
    private IForFrontSkuService dubboSkuService;

    // 根据SpuId查询所有规格的对象
    @DubboReference
    private IForFrontAttributeService dubboAttributeService;


    // 根据分类id分页查询商品列表
    @Override
    public JsonPage<SpuListItemVO> listSpuByCategoryId(
                                Long categoryId, Integer page, Integer pageSize) {
        // dubbo调用listSpuByCategoryId方法是写好分页的,我们只需直接调用即可
        JsonPage<SpuListItemVO> jsonPage =
                dubboSpuService.listSpuByCategoryId(categoryId, page, pageSize);
        // 别忘了返回jsonPage!!!
        return jsonPage;
    }


    // 根据spuId查询spu信息
    @Override
    public SpuStandardVO getFrontSpuById(Long id) {
        SpuStandardVO spuStandardVO=dubboSpuService.getSpuById(id);
        return spuStandardVO;
    }
    // 根据spuId查询sku列表
    @Override
    public List<SkuStandardVO> getFrontSkusBySpuId(Long spuId) {
        List<SkuStandardVO> list=dubboSkuService.getSkusBySpuId(spuId);
        return list;
    }
    // 根据spuId查询spuDetail的详情信息(商品详情图片)
    @Override
    public SpuDetailStandardVO getSpuDetail(Long spuId) {
        SpuDetailStandardVO spuDetailStandardVO =
                                dubboSpuService.getSpuDetailById(spuId);
        return spuDetailStandardVO;
    }
    // 根据spuId查询当前商品所有规格(使用了表连接:5表或3表)
    @Override
    public List<AttributeStandardVO> getSpuAttributesBySpuId(Long spuId) {
        List<AttributeStandardVO> list=dubboAttributeService
                .getSpuAttributesBySpuId(spuId);
        return list;
    }
}
